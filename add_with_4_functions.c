//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
float input (int n)
{
    float x; 
    printf("Enter a number %d \n",n);
    scanf("%f",&x);
    return x;
}

float total(float a, float b)
{
    float sum;
    sum = a+b;
    return sum;
}

float output(float a,float b, float c)
{
    printf("Sum of %.2f +%.2f is %.2f\n",a ,b ,c);
}

float main () 
{
    float a=input(1);
    float b=input(2);
    float sum=total(a, b);
    output(a, b, sum);
    return 0;
}