//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include <math.h>
struct point
{     
        float x;
        float y;
};
typedef struct point Point;
Point input();
float calc(Point,Point);
void output(Point, Point, float );
int main()
{
    float dis;
    Point a,b;
    a = input();
    b = input();
    dis = calc(a,b);
    output(a,b,dis);
    return 0;
}
Point input()
{
     Point p;
     printf("Enter the value for ordinate or x-axis: ");
     scanf("%f",&p.x);
     printf("Enter the value for abscissa or y-axis: ");
     scanf("%f",&p.y);
     return p;
}
float calc(Point a, Point b)
{
      float distance;
      distance = sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
      return distance;
}
void output(Point a, Point b, float dis)
{
      printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f",a.x,a.y,b.x,b.y,dis);
}
