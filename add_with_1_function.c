//Write a program to add two user input numbers using one function.
#include <stdio.h>
#include <conio.h>
int sum (int n1, int n2)
{
     return n1 + n2;
}
void main ()
{
     int n1, n2, total;
     printf("Enter a number: ");
     scanf("%d", &n1);
     printf("Enter a number: ");
     scanf("%d", &n2);
     total = sum(n1, n2);
     printf("The sum of %d and %d is %d", n1, n2, total);
}
