//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
float vol(float h,float d,float b)
{
	return ((h*d*b) + d/b) * 1/3;
}
 
int main()
{
    
        float h, d ,b;
      float volume;
      
      printf("Enter the value for height: ");
      scanf("%f", &h);
      printf("Enter the value for depth: ");
      scanf("%f", &d);
      printf("Enter the value for breadth: ");
      scanf("%f", &b);
      
      volume = vol(h, d, b);
      printf("Volume of tromboloid of height %f, depth %f and breadth %f is %f", h, d, b, volume);
      return volume;
  
}