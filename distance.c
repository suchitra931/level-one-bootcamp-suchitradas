//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
float insert (char x, int y);
float distance (float x1, float y1, float x2, float y2);
void display (float x1, float y1, float  x2, float y2, float dis);
int main()
{
     float x1, y1,x2,y2,d;
     x1 = insert ('x', 1);
     y1 = insert ('y', 1);
     x2 = insert ('x', 2);
     y2 = insert ('y', 2);
     d = distance (x1, y1, x2, y2);
     display (x1, y1, x2, y2, d);
     return 0;
}

float distance (float x1, float y1, float x2, float y2)
{ 
     float dis;
     dis = sqrt ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
     return dis;
}

float insert (char x, int y)
{ 
     float z;
     printf("Enter the point %c%d \n", x, y);
     scanf ("%f", &z);
     return z;
}

void display (float x1, float y1, float x2, float y2, float dis)
{
    printf("The distance between the two points (%.2f, %.2f) and (%.2f, %.2f) is %f", x1, y1, x2,   y2, dis);
}