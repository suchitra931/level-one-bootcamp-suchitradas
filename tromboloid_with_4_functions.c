//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input_height(float h)
{
    printf("Enter the value for height \n");
    scanf("%f", &h);
        while(h<0)
        {
             printf("Enter a positive value for height");
             scanf("%f", &h);
         }
        return h;
}
float input_depth(float d)
{
    printf("Enter the value for depth \n");
    scanf("%f", &d);
         while(d<=0)
          {
              printf("Enter a positive value for depth: ");
              scanf("%f", &d);
           }
         return d;
}
float input_breadth(float b)
{
    printf("Enter the value for breadth \n");
    scanf("%f", &b);
    while (b<=0)
    {
           printf("Enter the value for breadth: ");
           scanf("%f", &b);
    }
  return b;
}
float vol(float h, float d, float b)
{
     float volume;
     volume = ((h * d * b) + d/b) * 1/3;
     return volume;
}
float output(float h, float d, float b, float volume)
{
    printf("Volume of trompoloid of height %f, depth %f and breadth %f is %f" , h, d, b, volume);
}
int main()
{
float h = input_height(h);
float d = input_depth(d);
float b = input_breadth(b);
float volume = vol(h, d, b);
output(h, d, b, volume);
return volume;
}