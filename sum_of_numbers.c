//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int size()
{
      int a;
      printf("Enter the number of elements to be added: ");
      scanf("%d",&a);
      return a;
}
void array(int a,int arr[a])
{
      for (int i=0;i<a;i++)
      {
        printf("Enter element %d:",i+1);
        scanf("%d",&arr[i]);
      }
}
int sum(int a, int n[a])
{
    int total =0;
    for(int i=0;i<a;i++)
    {
      total = total + n[i];
    }
    return total;
}
void output(int total )
{
     printf("The sum of the elements entered are: %d ",total);
}
int main()
{
    int a,total;
    a = size();
    int n[a];
    array(a,n);
    total = sum(a,n);
    output(total);
    return 0;
}
